-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2021 at 10:23 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lpsoee_itsb`
--

-- --------------------------------------------------------

--
-- Table structure for table `actual_sequence`
--

CREATE TABLE `actual_sequence` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `shift_id` int(11) NOT NULL,
  `part_no_id` int(11) NOT NULL,
  `qty_plan` varchar(20) NOT NULL,
  `end_time_plan` time NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `actual_time` varchar(20) NOT NULL,
  `qty_total` varchar(20) NOT NULL,
  `gsph` varchar(20) NOT NULL,
  `total_dt` varchar(20) NOT NULL,
  `dies_change` varchar(20) NOT NULL,
  `dies_change_dt` varchar(20) NOT NULL,
  `no_production_dt` varchar(20) NOT NULL,
  `production_dt` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_availability`
--

CREATE TABLE `daily_availability` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_avai` varchar(25) NOT NULL,
  `ave_daily_avai` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_gsph`
--

CREATE TABLE `daily_gsph` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_gsph` varchar(10) NOT NULL,
  `ave_daily_gsph` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_oee`
--

CREATE TABLE `daily_oee` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time(6) NOT NULL,
  `ave_current_oee` varchar(25) NOT NULL,
  `ave_daily_oee` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_plan_achievement`
--

CREATE TABLE `daily_plan_achievement` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_achie` varchar(25) NOT NULL,
  `ave_daily_achie` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_speed`
--

CREATE TABLE `daily_speed` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_speed` varchar(25) NOT NULL,
  `ave_daily_speed` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_utilization`
--

CREATE TABLE `daily_utilization` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_uti` varchar(15) NOT NULL,
  `ave_daily_uti` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_yield`
--

CREATE TABLE `daily_yield` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `ave_current_yield` varchar(25) NOT NULL,
  `ave_daily_yield` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `downtime_report`
--

CREATE TABLE `downtime_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `downtime_period` varchar(20) NOT NULL,
  `problem_dies_change` int(11) DEFAULT NULL,
  `problem_die` int(11) DEFAULT NULL,
  `problem_machine` int(11) DEFAULT NULL,
  `problem_material` int(11) DEFAULT NULL,
  `problem_production` int(11) DEFAULT NULL,
  `problem_qc` int(11) DEFAULT NULL,
  `pic_id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fp_average`
--

CREATE TABLE `fp_average` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fp_dt_daily`
--

CREATE TABLE `fp_dt_daily` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fp_real_time`
--

CREATE TABLE `fp_real_time` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `max_speed` varchar(25) NOT NULL,
  `actual_speed` varchar(25) NOT NULL,
  `speed_loss` varchar(25) NOT NULL,
  `operation_state` varchar(25) NOT NULL,
  `downtime_state` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fp_setting`
--

CREATE TABLE `fp_setting` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(25) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `machine_info`
--

CREATE TABLE `machine_info` (
  `id` int(11) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `factory` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `machine_name` varchar(255) NOT NULL,
  `machine_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `performance`
--

CREATE TABLE `performance` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `shift_id` int(11) NOT NULL,
  `part_no_id` int(11) NOT NULL,
  `availability` varchar(25) NOT NULL,
  `speed` varchar(25) NOT NULL,
  `yield` varchar(25) NOT NULL,
  `gsph` varchar(25) NOT NULL,
  `achievement` varchar(25) NOT NULL,
  `oee` varchar(25) NOT NULL,
  `utilization` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `realtime_average`
--

CREATE TABLE `realtime_average` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `shift_id` int(11) NOT NULL,
  `availability` varchar(25) NOT NULL,
  `speed` varchar(25) NOT NULL,
  `yield` varchar(25) NOT NULL,
  `gsph` varchar(25) NOT NULL,
  `achievement` varchar(25) NOT NULL,
  `oee` varchar(25) NOT NULL,
  `utilization` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `real_time`
--

CREATE TABLE `real_time` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `plan_production` varchar(20) DEFAULT '0',
  `no_production` varchar(20) DEFAULT '0',
  `dies_change` varchar(20) DEFAULT '0',
  `production_dt` varchar(20) DEFAULT '0',
  `dies_change_dt` varchar(20) DEFAULT '0',
  `machine_dt` varchar(20) DEFAULT '0',
  `dies_dt` varchar(20) DEFAULT '0',
  `quality_dt` varchar(20) DEFAULT '0',
  `material_dt` varchar(20) DEFAULT '0',
  `part_no_id` int(11) NOT NULL,
  `qty_plan` varchar(20) NOT NULL DEFAULT '0',
  `shift_id` int(11) NOT NULL,
  `qty_total` varchar(25) NOT NULL DEFAULT '0',
  `qty_ok` varchar(25) NOT NULL DEFAULT '0',
  `qty_reject` varchar(25) NOT NULL DEFAULT '0',
  `good_footage` varchar(25) NOT NULL DEFAULT '0',
  `rejection_footage` varchar(25) NOT NULL DEFAULT '0',
  `total_dt` varchar(25) NOT NULL DEFAULT '0',
  `start_time` time NOT NULL,
  `qty_rework` varchar(25) NOT NULL DEFAULT '0',
  `qty_scrap` varchar(25) NOT NULL DEFAULT '0',
  `gsph` varchar(25) NOT NULL DEFAULT '0',
  `max_speed` varchar(20) NOT NULL DEFAULT '0',
  `actual_speed` varchar(20) NOT NULL DEFAULT '0',
  `speed_loss` varchar(20) NOT NULL DEFAULT '0',
  `operation_state` varchar(25) NOT NULL DEFAULT '0',
  `downtime_state` varchar(25) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rework_report`
--

CREATE TABLE `rework_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `actual_sequence_id` int(11) NOT NULL,
  `crack` varchar(10) NOT NULL,
  `necking` varchar(10) NOT NULL,
  `wrinkle` varchar(10) NOT NULL,
  `overlap` varchar(10) NOT NULL,
  `overcut` varchar(10) NOT NULL,
  `burr` varchar(10) NOT NULL,
  `mekure` varchar(10) NOT NULL,
  `scratch` varchar(10) NOT NULL,
  `vavy` varchar(10) NOT NULL,
  `oblong` varchar(10) NOT NULL,
  `offset` varchar(10) NOT NULL,
  `dent` varchar(10) NOT NULL,
  `hump` varchar(10) NOT NULL,
  `rusty` varchar(10) NOT NULL,
  `metal_chip` varchar(10) NOT NULL,
  `stamp_scrap` varchar(10) NOT NULL,
  `part_drop` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `scrap_report`
--

CREATE TABLE `scrap_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `actual_sequence_id` int(11) NOT NULL,
  `crack` varchar(10) NOT NULL,
  `necking` varchar(10) NOT NULL,
  `wrinkle` varchar(10) NOT NULL,
  `overlap` varchar(10) NOT NULL,
  `overcut` varchar(10) NOT NULL,
  `burr` varchar(10) NOT NULL,
  `mekure` varchar(10) NOT NULL,
  `scratch` varchar(10) NOT NULL,
  `vavy` varchar(10) NOT NULL,
  `oblong` varchar(10) NOT NULL,
  `offset` varchar(10) NOT NULL,
  `dent` varchar(10) NOT NULL,
  `hump` varchar(10) NOT NULL,
  `rusty` varchar(10) NOT NULL,
  `metal_chip` varchar(10) NOT NULL,
  `stamp_scrap` varchar(10) NOT NULL,
  `part_drop` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `type_defect`
--

CREATE TABLE `type_defect` (
  `id` int(11) NOT NULL,
  `defect_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_defect`
--

INSERT INTO `type_defect` (`id`, `defect_type`) VALUES
(1, 'CRACK'),
(2, 'NECKING'),
(3, 'WRINKLE'),
(4, 'OVERLAP'),
(5, 'OVERCUT'),
(6, 'BURR'),
(7, 'MEKURE'),
(8, 'SCRATCH'),
(9, 'VAVY'),
(10, 'OBLONG'),
(11, 'OFFSET'),
(12, 'DENT'),
(13, 'HUMP'),
(14, 'RUSTY'),
(15, 'METAL CHIP'),
(16, 'STAMP SCRAP'),
(17, 'PART DROP');

-- --------------------------------------------------------

--
-- Table structure for table `type_department`
--

CREATE TABLE `type_department` (
  `id` int(7) NOT NULL,
  `department_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_department`
--

INSERT INTO `type_department` (`id`, `department_name`) VALUES
(1, 'Machine'),
(2, 'Dies'),
(3, 'Production'),
(4, 'Dies Change'),
(5, 'Material'),
(6, 'QC');

-- --------------------------------------------------------

--
-- Table structure for table `type_dies_change_downtime`
--

CREATE TABLE `type_dies_change_downtime` (
  `id` int(11) NOT NULL,
  `dies_change_downtime` varchar(100) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_dies_change_downtime`
--

INSERT INTO `type_dies_change_downtime` (`id`, `dies_change_downtime`, `department_id`) VALUES
(1, 'Lambat setting die - prod', 4),
(2, 'Manpower low skill - prod', 4),
(3, 'Shortage manpower - prod', 4),
(4, 'Die problem - die', 4),
(5, 'Crane rosak - maintenance', 4),
(6, 'Press error - maintenance', 4);

-- --------------------------------------------------------

--
-- Table structure for table `type_die_downtime`
--

CREATE TABLE `type_die_downtime` (
  `id` int(11) NOT NULL,
  `die_downtime` varchar(100) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `type_machine_downtime`
--

CREATE TABLE `type_machine_downtime` (
  `id` int(11) NOT NULL,
  `machine_downtime_cause` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_machine_downtime`
--

INSERT INTO `type_machine_downtime` (`id`, `machine_downtime_cause`, `department_id`) VALUES
(1, 'Press machine;C1 mechanical', 1),
(2, 'Press machine;C1 control', 1),
(3, 'Press machine;C1 hydraulic', 1),
(4, 'Press machine;C1 pneumatic', 1),
(5, 'Press machine;C2 mechanical', 1),
(6, 'Press machine;C2 control', 1),
(7, 'Press machine;C2 hydraulic', 1),
(8, 'Press machine;C2 pneumatic', 1),
(9, 'Press machine;C3 mechanical', 1),
(10, 'Press machine;C3 control', 1),
(11, 'Press machine;C3 hydraulic', 1),
(12, 'Press machine;C3 pneumatic', 1),
(13, 'Robot', 1),
(14, 'Destacker', 1),
(15, 'Conveyor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `type_material_downtime`
--

CREATE TABLE `type_material_downtime` (
  `id` int(11) NOT NULL,
  `material_downtime` varchar(100) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_material_downtime`
--

INSERT INTO `type_material_downtime` (`id`, `material_downtime`, `department_id`) VALUES
(1, 'Material shortage - Receiving', 5),
(2, 'Material NG - SQA', 5),
(3, 'Material not follow std pkg - Receiving', 5);

-- --------------------------------------------------------

--
-- Table structure for table `type_part_list`
--

CREATE TABLE `type_part_list` (
  `id` int(11) NOT NULL,
  `part_no` varchar(255) DEFAULT NULL,
  `part_name` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `process` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `cycle_time_sec` float DEFAULT NULL,
  `target_gsph` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_part_list`
--

INSERT INTO `type_part_list` (`id`, `part_no`, `part_name`, `model`, `process`, `line`, `cycle_time_sec`, `target_gsph`) VALUES
(1, '57413-BZ040', 'MEMBER FLOOR SIDE INNER RR RH', 'ALZA', '4', 'C', 4.8, '688'),
(2, '57414-BZ040', 'MEMBER FLOOR SIDE INNER RR LH', 'ALZA', '4', 'C', 4.8, '688'),
(3, '57631-BZ020', 'RF RR FLOOR SIDE MMBR FR RH', 'ALZA', '4', 'C', 4.9, '673'),
(4, '57633/4-BZ020', 'EXTENSION RR FLR SD MBR. RR RH/LH', 'ALZA', '3', 'C', 4.8, '688'),
(5, '63135-BZ020', 'PANEL BACK WINDOW UPR INNER', 'ALZA', '3', 'C', 4.8, '688'),
(6, '57653-BZ100', 'PNL MBR RR FLOOR CROSS NO.2', 'ALZA', '3', 'C', 6.5, '508'),
(7, '57639-BZ020', 'RF RR FLR PANEL NO.1 RH', 'ALZA', '4', 'C', 5, '660'),
(8, '63141-BZ081', 'REINF, ROOF PANEL, CTR', 'ALZA', '3', 'C', 5.6, '589'),
(9, '63145-BZ040', 'REINF, WINDSHIELD HEADER PANEL', 'ALZA', '3', 'C', 5.7, '579'),
(10, '63143-BZ060', 'REINF, ROOF PANEL, NO.3', 'ALZA', '3', 'C', 5.7, '579'),
(11, '67337/8-BZ040', 'REINF, RR DOOR LOCK, RH', 'ALZA', '4', 'C', 4.9, '673'),
(12, '67585/6-BZ010', 'REINF, FR DOOR WINDOW, FR UPR RH', 'ALZA', '4', 'C', 4.9, '673'),
(13, '67335/6-BZ060', 'RNF FR DOOR LOCK RH', 'ALZA', '4', 'C', 4.8, '688'),
(14, '61413/4-BZ070', 'REINF ROCKER PANEL NO.2 RH', 'ALZA/MYVI', '', 'C', 5.6, '589'),
(15, '61625-BZ150', 'EXTENSION QTR PANEL RR RH', 'AXIA', '4', 'C', 4.6, '717'),
(16, '61636-BZ050', 'REINF QTR PANEL UPR LH', 'AXIA', '4', 'C', 5.7, '579'),
(17, '61635-BZ040', 'REINF QTR PANEL UPR RH', 'AXIA', '4', 'C', 5.7, '579'),
(18, '58372-BZ180', 'PANEL BODY LOWER BACK INR', 'AXIA', '4', 'C', 6.2, '532'),
(19, '61626-BZ150', 'EXTENSION QTR PANEL RR LH', 'AXIA', '4', 'C', 4.6, '717'),
(20, '58371-BZ040', 'PANEL BODY LOWER BACK OTR (extra hole)', 'AXIA', '3', 'C', 4.4, '750'),
(21, '57413-BZ070', 'MBR FLOOR SIDE INR RR RH', 'AXIA/BEZZA', '4', 'C', 4.8, '688'),
(22, '57414-BZ070', 'MBR FLOOR SIDE INR RR LH', 'AXIA/BEZZA', '4', 'C', 4.8, '688'),
(23, '57453/4-BZ080', 'MBR FR FLOOR CROSS SIDE RH', 'AXIA/BEZZA', '4', 'C', 6.2, '532'),
(24, '57465-BZ040', 'GUSSET, CTR FLOOR CROSS MBR', 'AXIA/BEZZA', '4', 'C', 5.5, '600'),
(25, '57413/4-BZ110', 'PNL MBR FLOOR SIDE INR RR RH/LH', 'Myvi', '4', 'C', 4.8, '688'),
(26, '61657/8-BZ050', 'BRCKT RR  SHOCK ABSORBER RH/LH', 'Myvi', '4', 'C', 4.9, '673'),
(27, '57653-BZ170', 'PNL MBR RR FLOOR CROSS NO.2', 'Myvi', '4', 'C', 6, '550'),
(28, '57633/4-BZ060', 'EXTENSION, RR FLR SIDE MEMBER RR RH/LH', 'Myvi', '4', 'C', 4.9, '673'),
(29, '57663/4-BZ040', 'PANEL RR FLOOR SIDE RR RH', 'Myvi', '4', 'C', 4.8, '688'),
(30, '58315/7-BZ090', 'EXT RR FLOOR SIDE PANEL FR RH', 'Myvi', '4', 'C', 6.2, '532'),
(31, '57145/6-BZ090', 'REINFORCEMENT, FR SIDE MEMBER, RR RH', 'ARUZ', '4', 'C', 5.5, '600'),
(32, '57181-BZ010', 'REINFORCEMENT, FR SIDE MBR, RR NO.1 RH', 'ARUZ', '2', 'C', 5, '660'),
(33, '57182-BZ010', 'REINFORCEMENT, FR SIDE MBR, RR NO.1 LH', 'ARUZ', '2', 'C', 5, '660'),
(34, '57185/6-BZ010', 'REINFORCEMENT, FR SIDE MBR, RR NO.1 INN RH /LH', 'ARUZ', '3', 'C', 5.2, '635'),
(35, '53737/8-BZ080', 'PLATE FR APRON TO COWL SIDE MEMBER RH', 'ARUZ', '3', 'C', 5, '660'),
(36, '57113/4-BZ020', 'MEMBER FR SIDE OUTER RH', 'ARUZ', '2', 'C', 4.6, '717'),
(37, '67445/6-BZ090', 'PLATE RR DOOR FR FRAME TO INS PNL RH', 'ARUZ', '4', 'C', 5, '660'),
(38, '67123-BZ180', 'BRACKET FR DOOR OUTSIDE PANEL RH', 'ARUZ', '3', 'C', 5, '660'),
(39, '67124-BZ150', 'BRACKET FR DOOR OUTSIDE PANEL LH', 'ARUZ', '3', 'C', 5, '660'),
(40, '67151-BZ140', 'REINF FR DOOR INSIDE PANEL RH', 'ARUZ', '3', 'C', 5, '660'),
(41, '67152-BZ140', 'REINF FR DOOR INSIDE PANEL LH', 'ARUZ', '3', 'C', 5, '660'),
(42, '67335/6-BZ180', 'REINF FR DOOR LOCK RH', 'ARUZ', '4', 'C', 4.9, '673'),
(43, 'PW838995/6', 'EXT SIDE SILL REINF LH', 'EXORA', '4', 'C', 5.6, '589'),
(44, 'PW931226', 'EXT REINF RAIL ROOF SIDE RH', 'PESONA', '3', 'C', 5.9, '559'),
(45, 'PW931225', 'EXT REINF RAIL ROOF SIDE LH', 'PESONA', '3', 'C', 5.9, '559'),
(46, 'PW935545/6', 'PANEL BACK UPPER SIDE LH/RH', 'PESONA', '4', 'C', 6, '550'),
(47, 'PW933391/2', 'EXTENSION QTR INR LWR LH/RH', 'IRIZ', '4', 'C', 5.8, '569'),
(48, 'PW932427', 'REINF FRONT DOOR HINGE UPR LH', 'IRIZ/PESONA', '4', 'C', 5.8, '569'),
(49, 'PW932428', 'REINF FRONT DOOR HINGE UPR RH', 'IRIZ/PESONA', '4', 'C', 5.8, '569'),
(50, 'PW932434/3', 'REINF FRONT DOOR HINGE LWR RH/LH', 'IRIZ/PESONA', '4', 'C', 5.8, '569'),
(51, 'PW932451', 'RAIL FOOF FRONT', 'IRIZ/PESONA', '4', 'C', 5.8, '569');

-- --------------------------------------------------------

--
-- Table structure for table `type_pattern`
--

CREATE TABLE `type_pattern` (
  `id` int(11) NOT NULL,
  `pattern_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `type_production_downtime`
--

CREATE TABLE `type_production_downtime` (
  `id` int(11) NOT NULL,
  `production_downtime` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_production_downtime`
--

INSERT INTO `type_production_downtime` (`id`, `production_downtime`, `department_id`) VALUES
(1, 'Downtime Investigation', 3),
(2, 'Part Conformation', 3),
(3, 'Trial', 3),
(4, 'Vacuum error;Vacuum koyak', 3),
(5, 'Vacuum error;Level compasitor jammed', 3),
(6, 'Vacuum error;Hose bocor', 3),
(7, 'Vacuum error;L-Bow patah', 3),
(8, 'Vacuum error;Ikat finger tak kuat', 3),
(9, 'Vacuum error;Finger goyang', 3),
(10, 'Vacuum error;Team lambat masuk', 3),
(11, 'Vacuum error;Salah setting finger', 3),
(12, 'Vacuum error;Salah setting die', 3),
(13, 'Manpower shortage;Shortage', 3),
(14, 'Manpower shortage;AL', 3),
(15, 'Manpower shortage;MC', 3),
(16, 'Manpower shortage;EL', 3),
(17, 'Manpower shortage;ABS', 3),
(18, 'Manpower shortage;CL', 3),
(19, 'Robot teaching;R1', 3),
(20, 'Robot teaching;R2 RH', 3),
(21, 'Robot teaching;R2 LH', 3),
(22, 'Robot teaching;R3 RH', 3),
(23, 'Robot teaching;R3 LH', 3),
(24, 'Robot teaching;R4 RH', 3),
(25, 'Robot teaching;R4 LH', 3),
(26, 'Robot teaching;R5 RH', 3),
(27, 'Robot teaching;R5 LH', 3),
(28, 'Robot teaching;R6', 3);

-- --------------------------------------------------------

--
-- Table structure for table `type_qc_downtime`
--

CREATE TABLE `type_qc_downtime` (
  `id` int(11) NOT NULL,
  `qc_downtime` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_qc_downtime`
--

INSERT INTO `type_qc_downtime` (`id`, `qc_downtime`, `department_id`) VALUES
(1, 'Accuracy Check', 6),
(2, 'Part Confirmation', 6);

-- --------------------------------------------------------

--
-- Table structure for table `type_role`
--

CREATE TABLE `type_role` (
  `id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_role`
--

INSERT INTO `type_role` (`id`, `role`) VALUES
(1, 'Manager'),
(2, 'Engineer'),
(3, 'Supervisor'),
(4, 'Leader');

-- --------------------------------------------------------

--
-- Table structure for table `type_shift`
--

CREATE TABLE `type_shift` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_shift`
--

INSERT INTO `type_shift` (`id`, `shift_name`) VALUES
(1, 'A'),
(2, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `ic_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_actual_seq`
-- (See below for the actual view)
--
CREATE TABLE `v_actual_seq` (
`id` int(11)
,`date` date
,`shift_name` varchar(20)
,`part_no` varchar(255)
,`part_name` varchar(255)
,`qty_plan` varchar(20)
,`end_time_plan` time
,`start_time` datetime
,`end_time` datetime
,`actual_time` varchar(20)
,`qty_total` varchar(20)
,`gsph` varchar(20)
,`total_dt` varchar(20)
,`dies_change` varchar(20)
,`dies_change_dt` varchar(20)
,`no_production_dt` varchar(20)
,`production_dt` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_current_part`
-- (See below for the actual view)
--
CREATE TABLE `v_current_part` (
`id` int(11)
,`date` date
,`part_no` varchar(255)
,`part_name` varchar(255)
,`line` varchar(255)
,`model` varchar(255)
,`process` varchar(255)
,`cycle_time_sec` float
,`qty_rework` varchar(25)
,`qty_scrap` varchar(25)
,`qty_ok` varchar(25)
,`gsph` varchar(25)
,`qty_total` varchar(25)
,`shift_name` varchar(20)
,`qty_plan` varchar(20)
,`start_time` time
,`max_speed` varchar(20)
,`actual_speed` varchar(20)
,`speed_loss` varchar(20)
,`target_gsph` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_current_realtime`
-- (See below for the actual view)
--
CREATE TABLE `v_current_realtime` (
`Id` int(11)
,`date` date
,`part_no_id` int(11)
,`shift_id` int(11)
,`max_speed` varchar(25)
,`actual_speed` varchar(25)
,`speed_loss` varchar(25)
,`operation_state` varchar(25)
,`downtime_state` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_availability`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_availability` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_avai` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_gsph`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_gsph` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_gsph` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_oee`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_oee` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_oee` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_plan_achievement`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_plan_achievement` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_achie` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_speed`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_speed` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_speed` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_utilization`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_utilization` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_uti` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_yield`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_yield` (
`id` int(11)
,`date` date
,`time` time
,`shift_id` int(11)
,`part_no_id` int(11)
,`current_yield` varchar(25)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_downtime_report`
-- (See below for the actual view)
--
CREATE TABLE `v_downtime_report` (
`date` date
,`shift_name` varchar(20)
,`part_no` varchar(255)
,`part_name` varchar(255)
,`start_time` time
,`downtime_period` varchar(20)
,`dies_change_downtime` varchar(100)
,`die_downtime` varchar(100)
,`machine_downtime_cause` varchar(255)
,`material_downtime` varchar(100)
,`production_downtime` varchar(255)
,`qc_downtime` varchar(255)
,`department_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_front_panel`
-- (See below for the actual view)
--
CREATE TABLE `v_front_panel` (
`date` date
,`shift_id` int(11)
,`part_no_id` int(11)
,`plan_production` decimal(44,6)
,`no_production` decimal(44,6)
,`dies_change` decimal(44,6)
,`production_dt` decimal(44,6)
,`dies_change_dt` decimal(44,6)
,`machine_dt` decimal(44,6)
,`dies_dt` decimal(44,6)
,`quality_dt` decimal(44,6)
,`material_dt` decimal(44,6)
,`qty_plan` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_realtime`
-- (See below for the actual view)
--
CREATE TABLE `v_realtime` (
`date` date
,`plan_production` double
,`no_production` double
,`dies_change` double
,`production_dt` double
,`dies_change_dt` double
,`machine_dt` double
,`dies_dt` double
,`quality_dt` double
,`material_dt` double
,`qty_plan` double
,`qty_total` double
,`qty_ok` double
,`qty_reject` double
,`good_footage` double
,`rejection_footage` double
,`total_dt` double
,`start_time` decimal(29,0)
,`qty_rework` double
,`qty_scrap` double
,`gsph` varchar(25)
,`max_speed` double
,`actual_speed` double
,`speed_loss` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_rest_dt_report`
-- (See below for the actual view)
--
CREATE TABLE `v_rest_dt_report` (
`id` int(11)
,`date` date
,`part_no_id` int(7)
,`shift_id` int(7)
,`time` time
,`downtime_period` varchar(20)
,`problem_dies_change` varchar(11)
,`problem_die` varchar(11)
,`problem_machine` varchar(11)
,`problem_material` varchar(11)
,`problem_production` varchar(11)
,`problem_qc` varchar(11)
,`pic_id` int(7)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_rework`
-- (See below for the actual view)
--
CREATE TABLE `v_rework` (
`id` int(11)
,`date` date
,`shift_name` varchar(20)
,`part_no` varchar(255)
,`part_name` varchar(255)
,`crack` varchar(10)
,`necking` varchar(10)
,`wrinkle` varchar(10)
,`overlap` varchar(10)
,`overcut` varchar(10)
,`burr` varchar(10)
,`mekure` varchar(10)
,`scratch` varchar(10)
,`vavy` varchar(10)
,`oblong` varchar(10)
,`offset` varchar(10)
,`dent` varchar(10)
,`hump` varchar(10)
,`rusty` varchar(10)
,`metal_chip` varchar(10)
,`stamp_scrap` varchar(10)
,`part_drop` varchar(10)
,`sum_rework` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_scrap`
-- (See below for the actual view)
--
CREATE TABLE `v_scrap` (
`id` int(11)
,`date` date
,`shift_name` varchar(20)
,`part_no` varchar(255)
,`part_name` varchar(255)
,`crack` varchar(10)
,`necking` varchar(10)
,`wrinkle` varchar(10)
,`overlap` varchar(10)
,`overcut` varchar(10)
,`burr` varchar(10)
,`mekure` varchar(10)
,`scratch` varchar(10)
,`vavy` varchar(10)
,`oblong` varchar(10)
,`offset` varchar(10)
,`dent` varchar(10)
,`hump` varchar(10)
,`rusty` varchar(10)
,`metal_chip` varchar(10)
,`stamp_scrap` varchar(10)
,`part_drop` varchar(10)
,`sum_scrap` double
);

-- --------------------------------------------------------

--
-- Structure for view `v_actual_seq`
--
DROP TABLE IF EXISTS `v_actual_seq`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_actual_seq`  AS  select `actual_sequence`.`id` AS `id`,`actual_sequence`.`date` AS `date`,`type_shift`.`shift_name` AS `shift_name`,`type_part_list`.`part_no` AS `part_no`,`type_part_list`.`part_name` AS `part_name`,`actual_sequence`.`qty_plan` AS `qty_plan`,`actual_sequence`.`end_time_plan` AS `end_time_plan`,`actual_sequence`.`start_time` AS `start_time`,`actual_sequence`.`end_time` AS `end_time`,`actual_sequence`.`actual_time` AS `actual_time`,`actual_sequence`.`qty_total` AS `qty_total`,`actual_sequence`.`gsph` AS `gsph`,`actual_sequence`.`total_dt` AS `total_dt`,`actual_sequence`.`dies_change` AS `dies_change`,`actual_sequence`.`dies_change_dt` AS `dies_change_dt`,`actual_sequence`.`no_production_dt` AS `no_production_dt`,`actual_sequence`.`production_dt` AS `production_dt` from ((`actual_sequence` join `type_shift` on(`actual_sequence`.`shift_id` = `type_shift`.`id`)) join `type_part_list` on(`actual_sequence`.`part_no_id` = `type_part_list`.`id`)) order by `actual_sequence`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_current_part`
--
DROP TABLE IF EXISTS `v_current_part`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_current_part`  AS  select `real_time`.`id` AS `id`,`real_time`.`date` AS `date`,`type_part_list`.`part_no` AS `part_no`,`type_part_list`.`part_name` AS `part_name`,`type_part_list`.`line` AS `line`,`type_part_list`.`model` AS `model`,`type_part_list`.`process` AS `process`,`type_part_list`.`cycle_time_sec` AS `cycle_time_sec`,`real_time`.`qty_rework` AS `qty_rework`,`real_time`.`qty_scrap` AS `qty_scrap`,`real_time`.`qty_ok` AS `qty_ok`,`real_time`.`gsph` AS `gsph`,`real_time`.`qty_total` AS `qty_total`,`type_shift`.`shift_name` AS `shift_name`,`real_time`.`qty_plan` AS `qty_plan`,`real_time`.`start_time` AS `start_time`,`real_time`.`max_speed` AS `max_speed`,`real_time`.`actual_speed` AS `actual_speed`,`real_time`.`speed_loss` AS `speed_loss`,`type_part_list`.`target_gsph` AS `target_gsph` from ((`real_time` join `type_shift` on(`real_time`.`shift_id` = `type_shift`.`id`)) join `type_part_list` on(`real_time`.`part_no_id` = `type_part_list`.`id`)) order by `real_time`.`id` desc limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_current_realtime`
--
DROP TABLE IF EXISTS `v_current_realtime`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_current_realtime`  AS  select `fp_real_time`.`id` AS `Id`,`fp_real_time`.`date` AS `date`,`fp_real_time`.`part_no_id` AS `part_no_id`,`fp_real_time`.`shift_id` AS `shift_id`,`fp_real_time`.`max_speed` AS `max_speed`,`fp_real_time`.`actual_speed` AS `actual_speed`,`fp_real_time`.`speed_loss` AS `speed_loss`,`fp_real_time`.`operation_state` AS `operation_state`,`fp_real_time`.`downtime_state` AS `downtime_state` from `fp_real_time` ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_availability`
--
DROP TABLE IF EXISTS `v_daily_availability`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_availability`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`availability` AS `current_avai` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_gsph`
--
DROP TABLE IF EXISTS `v_daily_gsph`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_gsph`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`gsph` AS `current_gsph` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_oee`
--
DROP TABLE IF EXISTS `v_daily_oee`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_oee`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`oee` AS `current_oee` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_plan_achievement`
--
DROP TABLE IF EXISTS `v_daily_plan_achievement`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_plan_achievement`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`achievement` AS `current_achie` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_speed`
--
DROP TABLE IF EXISTS `v_daily_speed`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_speed`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`speed` AS `current_speed` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_utilization`
--
DROP TABLE IF EXISTS `v_daily_utilization`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_utilization`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`utilization` AS `current_uti` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_yield`
--
DROP TABLE IF EXISTS `v_daily_yield`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_yield`  AS  select `performance`.`id` AS `id`,`performance`.`date` AS `date`,`performance`.`time` AS `time`,`performance`.`shift_id` AS `shift_id`,`performance`.`part_no_id` AS `part_no_id`,`performance`.`yield` AS `current_yield` from (`performance` join `fp_average` on(`performance`.`date` = `fp_average`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_downtime_report`
--
DROP TABLE IF EXISTS `v_downtime_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_downtime_report`  AS  select `downtime_report`.`date` AS `date`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,ifnull(`type_part_list`.`part_no`,'-') AS `part_no`,ifnull(`type_part_list`.`part_name`,'-') AS `part_name`,`downtime_report`.`time` AS `start_time`,`downtime_report`.`downtime_period` AS `downtime_period`,ifnull(`type_dies_change_downtime`.`dies_change_downtime`,'-') AS `dies_change_downtime`,ifnull(`type_die_downtime`.`die_downtime`,'-') AS `die_downtime`,ifnull(`type_machine_downtime`.`machine_downtime_cause`,'-') AS `machine_downtime_cause`,ifnull(`type_material_downtime`.`material_downtime`,'-') AS `material_downtime`,ifnull(`type_production_downtime`.`production_downtime`,'-') AS `production_downtime`,ifnull(`type_qc_downtime`.`qc_downtime`,'-') AS `qc_downtime`,ifnull(`type_department`.`department_name`,NULL) AS `department_name` from (((((((((`downtime_report` left join `type_part_list` on(`downtime_report`.`part_no_id` = `type_part_list`.`id`)) left join `type_shift` on(`downtime_report`.`shift_id` = `type_shift`.`id`)) left join `type_dies_change_downtime` on(`downtime_report`.`problem_dies_change` = `type_dies_change_downtime`.`id`)) left join `type_die_downtime` on(`downtime_report`.`problem_die` = `type_die_downtime`.`id`)) left join `type_machine_downtime` on(`downtime_report`.`problem_machine` = `type_machine_downtime`.`id`)) left join `type_material_downtime` on(`downtime_report`.`problem_material` = `type_material_downtime`.`id`)) left join `type_production_downtime` on(`downtime_report`.`problem_production` = `type_production_downtime`.`id`)) left join `type_qc_downtime` on(`downtime_report`.`problem_qc` = `type_qc_downtime`.`id`)) left join `type_department` on(`downtime_report`.`pic_id` = `type_department`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_front_panel`
--
DROP TABLE IF EXISTS `v_front_panel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_front_panel`  AS  select `real_time`.`date` AS `date`,`real_time`.`shift_id` AS `shift_id`,`real_time`.`part_no_id` AS `part_no_id`,sum(time_to_sec(ifnull(`real_time`.`plan_production`,0))) AS `plan_production`,sum(time_to_sec(ifnull(`real_time`.`no_production`,0))) AS `no_production`,sum(time_to_sec(ifnull(`real_time`.`dies_change`,0))) AS `dies_change`,sum(time_to_sec(ifnull(`real_time`.`production_dt`,0))) AS `production_dt`,sum(time_to_sec(ifnull(`real_time`.`dies_change_dt`,0))) AS `dies_change_dt`,sum(time_to_sec(ifnull(`real_time`.`machine_dt`,0))) AS `machine_dt`,sum(time_to_sec(ifnull(`real_time`.`dies_dt`,0))) AS `dies_dt`,sum(time_to_sec(ifnull(`real_time`.`quality_dt`,0))) AS `quality_dt`,sum(time_to_sec(ifnull(`real_time`.`material_dt`,0))) AS `material_dt`,`real_time`.`qty_plan` AS `qty_plan` from (`real_time` join `fp_setting` on(`real_time`.`date` = `fp_setting`.`date` and `real_time`.`part_no_id` = `fp_setting`.`part_no_id` and `real_time`.`shift_id` = `fp_setting`.`shift_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_realtime`
--
DROP TABLE IF EXISTS `v_realtime`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_realtime`  AS  select `real_time`.`date` AS `date`,sum(`real_time`.`plan_production`) AS `plan_production`,sum(`real_time`.`no_production`) AS `no_production`,sum(`real_time`.`dies_change`) AS `dies_change`,sum(`real_time`.`production_dt`) AS `production_dt`,sum(`real_time`.`dies_change_dt`) AS `dies_change_dt`,sum(`real_time`.`machine_dt`) AS `machine_dt`,sum(`real_time`.`dies_dt`) AS `dies_dt`,sum(`real_time`.`quality_dt`) AS `quality_dt`,sum(`real_time`.`material_dt`) AS `material_dt`,sum(`real_time`.`qty_plan`) AS `qty_plan`,sum(`real_time`.`qty_total`) AS `qty_total`,sum(`real_time`.`qty_ok`) AS `qty_ok`,sum(`real_time`.`qty_reject`) AS `qty_reject`,sum(`real_time`.`good_footage`) AS `good_footage`,sum(`real_time`.`rejection_footage`) AS `rejection_footage`,sum(`real_time`.`total_dt`) AS `total_dt`,sum(`real_time`.`start_time`) AS `start_time`,sum(`real_time`.`qty_rework`) AS `qty_rework`,sum(`real_time`.`qty_scrap`) AS `qty_scrap`,max(`real_time`.`gsph`) AS `gsph`,sum(`real_time`.`max_speed`) AS `max_speed`,sum(`real_time`.`actual_speed`) AS `actual_speed`,sum(`real_time`.`speed_loss`) AS `speed_loss` from (`real_time` join `fp_real_time` on(`real_time`.`date` = `fp_real_time`.`date`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_rest_dt_report`
--
DROP TABLE IF EXISTS `v_rest_dt_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_rest_dt_report`  AS  select `downtime_report`.`id` AS `id`,`downtime_report`.`date` AS `date`,`downtime_report`.`part_no_id` AS `part_no_id`,`downtime_report`.`shift_id` AS `shift_id`,`downtime_report`.`time` AS `time`,`downtime_report`.`downtime_period` AS `downtime_period`,ifnull(`downtime_report`.`problem_dies_change`,'-') AS `problem_dies_change`,ifnull(`downtime_report`.`problem_die`,'-') AS `problem_die`,ifnull(`downtime_report`.`problem_machine`,'-') AS `problem_machine`,ifnull(`downtime_report`.`problem_material`,'-') AS `problem_material`,ifnull(`downtime_report`.`problem_production`,'-') AS `problem_production`,ifnull(`downtime_report`.`problem_qc`,'-') AS `problem_qc`,`downtime_report`.`pic_id` AS `pic_id` from `downtime_report` order by `downtime_report`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_rework`
--
DROP TABLE IF EXISTS `v_rework`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_rework`  AS  select `rework_report`.`id` AS `id`,`rework_report`.`date` AS `date`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,ifnull(`type_part_list`.`part_no`,NULL) AS `part_no`,ifnull(`type_part_list`.`part_name`,NULL) AS `part_name`,`rework_report`.`crack` AS `crack`,`rework_report`.`necking` AS `necking`,`rework_report`.`wrinkle` AS `wrinkle`,`rework_report`.`overlap` AS `overlap`,`rework_report`.`overcut` AS `overcut`,`rework_report`.`burr` AS `burr`,`rework_report`.`mekure` AS `mekure`,`rework_report`.`scratch` AS `scratch`,`rework_report`.`vavy` AS `vavy`,`rework_report`.`oblong` AS `oblong`,`rework_report`.`offset` AS `offset`,`rework_report`.`dent` AS `dent`,`rework_report`.`hump` AS `hump`,`rework_report`.`rusty` AS `rusty`,`rework_report`.`metal_chip` AS `metal_chip`,`rework_report`.`stamp_scrap` AS `stamp_scrap`,`rework_report`.`part_drop` AS `part_drop`,sum(`rework_report`.`crack`) + sum(`rework_report`.`necking`) + sum(`rework_report`.`wrinkle`) + sum(`rework_report`.`overlap`) + sum(`rework_report`.`overcut`) + sum(`rework_report`.`burr`) + sum(`rework_report`.`mekure`) + sum(`rework_report`.`scratch`) + sum(`rework_report`.`vavy`) + sum(`rework_report`.`oblong`) + sum(`rework_report`.`offset`) + sum(`rework_report`.`dent`) + sum(`rework_report`.`hump`) + sum(`rework_report`.`rusty`) + sum(`rework_report`.`metal_chip`) + sum(`rework_report`.`stamp_scrap`) + sum(`rework_report`.`part_drop`) AS `sum_rework` from ((`rework_report` join `type_part_list` on(`rework_report`.`part_no_id` = `type_part_list`.`id`)) join `type_shift` on(`rework_report`.`shift_id` = `type_shift`.`id`)) group by `rework_report`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_scrap`
--
DROP TABLE IF EXISTS `v_scrap`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_scrap`  AS  select `scrap_report`.`id` AS `id`,`scrap_report`.`date` AS `date`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,ifnull(`type_part_list`.`part_no`,NULL) AS `part_no`,ifnull(`type_part_list`.`part_name`,NULL) AS `part_name`,`scrap_report`.`crack` AS `crack`,`scrap_report`.`necking` AS `necking`,`scrap_report`.`wrinkle` AS `wrinkle`,`scrap_report`.`overlap` AS `overlap`,`scrap_report`.`overcut` AS `overcut`,`scrap_report`.`burr` AS `burr`,`scrap_report`.`mekure` AS `mekure`,`scrap_report`.`scratch` AS `scratch`,`scrap_report`.`vavy` AS `vavy`,`scrap_report`.`oblong` AS `oblong`,`scrap_report`.`offset` AS `offset`,`scrap_report`.`dent` AS `dent`,`scrap_report`.`hump` AS `hump`,`scrap_report`.`rusty` AS `rusty`,`scrap_report`.`metal_chip` AS `metal_chip`,`scrap_report`.`stamp_scrap` AS `stamp_scrap`,`scrap_report`.`part_drop` AS `part_drop`,sum(`scrap_report`.`crack`) + sum(`scrap_report`.`necking`) + sum(`scrap_report`.`wrinkle`) + sum(`scrap_report`.`overlap`) + sum(`scrap_report`.`overcut`) + sum(`scrap_report`.`burr`) + sum(`scrap_report`.`mekure`) + sum(`scrap_report`.`scratch`) + sum(`scrap_report`.`vavy`) + sum(`scrap_report`.`oblong`) + sum(`scrap_report`.`offset`) + sum(`scrap_report`.`dent`) + sum(`scrap_report`.`hump`) + sum(`scrap_report`.`rusty`) + sum(`scrap_report`.`metal_chip`) + sum(`scrap_report`.`stamp_scrap`) + sum(`scrap_report`.`part_drop`) AS `sum_scrap` from ((`scrap_report` join `type_part_list` on(`scrap_report`.`part_no_id` = `type_part_list`.`id`)) join `type_shift` on(`scrap_report`.`shift_id` = `type_shift`.`id`)) group by `scrap_report`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `daily_availability`
--
ALTER TABLE `daily_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_gsph`
--
ALTER TABLE `daily_gsph`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_oee`
--
ALTER TABLE `daily_oee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_plan_achievement`
--
ALTER TABLE `daily_plan_achievement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_speed`
--
ALTER TABLE `daily_speed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_utilization`
--
ALTER TABLE `daily_utilization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_yield`
--
ALTER TABLE `daily_yield`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downtime_report`
--
ALTER TABLE `downtime_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `problem_dies_change` (`problem_dies_change`),
  ADD KEY `problem_machine` (`problem_machine`),
  ADD KEY `problem_material` (`problem_material`),
  ADD KEY `problem_production` (`problem_production`),
  ADD KEY `problem_qc` (`problem_qc`),
  ADD KEY `pic_id` (`pic_id`),
  ADD KEY `part_no_id` (`part_no_id`) USING BTREE,
  ADD KEY `problem_die` (`problem_die`) USING BTREE;

--
-- Indexes for table `fp_average`
--
ALTER TABLE `fp_average`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fp_dt_daily`
--
ALTER TABLE `fp_dt_daily`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fp_real_time`
--
ALTER TABLE `fp_real_time`
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `fp_setting`
--
ALTER TABLE `fp_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_info`
--
ALTER TABLE `machine_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `performance`
--
ALTER TABLE `performance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `realtime_average`
--
ALTER TABLE `realtime_average`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `real_time`
--
ALTER TABLE `real_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `rework_report`
--
ALTER TABLE `rework_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `rework_report_ibfk_3` (`actual_sequence_id`);

--
-- Indexes for table `scrap_report`
--
ALTER TABLE `scrap_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `scrap_report_ibfk_3` (`actual_sequence_id`);

--
-- Indexes for table `type_defect`
--
ALTER TABLE `type_defect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_department`
--
ALTER TABLE `type_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_part_list`
--
ALTER TABLE `type_part_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_pattern`
--
ALTER TABLE `type_pattern`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_role`
--
ALTER TABLE `type_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_shift`
--
ALTER TABLE `type_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `downtime_report`
--
ALTER TABLE `downtime_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fp_average`
--
ALTER TABLE `fp_average`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fp_dt_daily`
--
ALTER TABLE `fp_dt_daily`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fp_setting`
--
ALTER TABLE `fp_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `machine_info`
--
ALTER TABLE `machine_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performance`
--
ALTER TABLE `performance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `real_time`
--
ALTER TABLE `real_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rework_report`
--
ALTER TABLE `rework_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scrap_report`
--
ALTER TABLE `scrap_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_defect`
--
ALTER TABLE `type_defect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `type_department`
--
ALTER TABLE `type_department`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type_part_list`
--
ALTER TABLE `type_part_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `type_pattern`
--
ALTER TABLE `type_pattern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_role`
--
ALTER TABLE `type_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `type_shift`
--
ALTER TABLE `type_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  ADD CONSTRAINT `actual_sequence_ibfk_2` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `actual_sequence_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `downtime_report`
--
ALTER TABLE `downtime_report`
  ADD CONSTRAINT `downtime_report_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `downtime_report_ibfk_2` FOREIGN KEY (`pic_id`) REFERENCES `type_department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `downtime_report_ibfk_3` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_4` FOREIGN KEY (`problem_dies_change`) REFERENCES `type_dies_change_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_5` FOREIGN KEY (`problem_die`) REFERENCES `type_die_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_6` FOREIGN KEY (`problem_machine`) REFERENCES `type_machine_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_7` FOREIGN KEY (`problem_material`) REFERENCES `type_material_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_8` FOREIGN KEY (`problem_production`) REFERENCES `type_production_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_9` FOREIGN KEY (`problem_qc`) REFERENCES `type_qc_downtime` (`id`);

--
-- Constraints for table `fp_real_time`
--
ALTER TABLE `fp_real_time`
  ADD CONSTRAINT `fp_real_time_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fp_real_time_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `performance`
--
ALTER TABLE `performance`
  ADD CONSTRAINT `performance_ibfk_1` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `performance_ibfk_2` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `realtime_average`
--
ALTER TABLE `realtime_average`
  ADD CONSTRAINT `realtime_average_ibfk_1` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `real_time`
--
ALTER TABLE `real_time`
  ADD CONSTRAINT `real_time_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `real_time_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rework_report`
--
ALTER TABLE `rework_report`
  ADD CONSTRAINT `rework_report_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rework_report_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rework_report_ibfk_3` FOREIGN KEY (`actual_sequence_id`) REFERENCES `actual_sequence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `scrap_report`
--
ALTER TABLE `scrap_report`
  ADD CONSTRAINT `scrap_report_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scrap_report_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `scrap_report_ibfk_3` FOREIGN KEY (`actual_sequence_id`) REFERENCES `actual_sequence` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  ADD CONSTRAINT `type_dies_change_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  ADD CONSTRAINT `type_die_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  ADD CONSTRAINT `type_machine_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  ADD CONSTRAINT `type_material_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  ADD CONSTRAINT `type_production_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  ADD CONSTRAINT `type_qc_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `type_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
